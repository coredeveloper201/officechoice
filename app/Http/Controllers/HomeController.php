<?php

namespace App\Http\Controllers;

use App\Object20;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function index(Request $request)
    {


        $suppliers = DB::table('ocl_supplier')->select('supplierName', 'supplierCode')->get();

        // $input['search'] = request('search') ? request('search') : '';

        // $search_result = Object20::select('object_20.*','ocl_itemsupplier.*')
        //                 ->leftJoin('ocl_itemsupplier', 'ocl_itemsupplier.itemCode', 'object_20.itemCode')
        //                 ->where(function ($q) use ($input) {
        //                     if (strlen($input['search']) > 0) {
        //                         $q->where('object_20.descriptionone', 'Like', '%' . $input['search'] . '%');
        //                     }
        //                 })->paginate(20);


        return view('home')->with([
            'suppliers' => $suppliers,
           
        ]);


    }


    public function productDetails()
    {
        $suppliers = DB::table('ocl_supplier')->select('supplierName', 'supplierCode')->get();


           return view('productDetails')->with([
            'suppliers' => $suppliers,
           
        ]);
    }


    public function showInvoice()
    {
        
       return view('printInvoice');
    }


}
