<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
   public function showResetForm($token)
   {

       $user = User::where('resetPin', $token)->first();

       if($user){
           return view('auth.resetPasswordForm' , compact('token'));
       } else {
           return redirect()->route('password.request');
       }

   }

    public function reset($token)
    {


        $user = User::where('resetPin', $token)->first();

        if($user){
            $validatedData = request()->validate([
                'password' => 'required:min:6'
            ]);

            $password = request('password');
            $password_match = request('password_match');

            if($password != $password_match){
                return back()->with('warning' , "Both Password doesn't match");
            } else {
                $token = Str::random(40);
                $user->resetPin = $token;
                $user->userPassword = bcrypt($password);
                $user->save();

                return redirect()->route('login')->with('success' , 'You Passowrd is updated, Please login');
            }

        } else {
            return redirect()->route('password.request');
        }
    }
}
