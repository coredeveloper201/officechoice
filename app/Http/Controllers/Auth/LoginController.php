<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function Login(Request $request)
    {

        $validatedData = request()->validate([
            'userName' => 'required',
            'userPassword' => 'required'
        ]);

        $userName = request('userName');
        $userPassword = request('userPassword');

        $user =  User::where('userName' , $userName)->first();
        if($user){
            if (Hash::check($userPassword, $user->userPassword)) {
                Auth::loginUsingId($user->userId);
                return redirect('products');
            }
            else{
                return back()->with('warning' , "Username and password combination doesn't match");
            }
        } else {
            return back()->with('warning' , "Username and password combination doesn't match");
        }


    }
}
