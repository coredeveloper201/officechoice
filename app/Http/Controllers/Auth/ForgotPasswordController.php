<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{

    public function showLinkRequestForm()
    {
        return view('Auth.resetPassword');
    }


    public function sendResetLinkEmail(Request $request)
    {
        $email_or_user_name = request('reset_pass_email');

        if(trim($email_or_user_name) == ''){
            return back()->with('warning' , "Please insert Either Email or username");
        }

        $user =  User::where('userName' , $email_or_user_name)->orWhere('userEmail', $email_or_user_name)->first();

        if($user){

            $token = Str::random(40);

            $data['token'] = $token;
            $data['email'] = $user->userEmail;
            $data['name'] = $user->userName;

            $user->resetPin = $token;
            $user->save();

            Mail::send('mails.resetPasswordMail', $data, function($message) use ($data){
                $message->to($data['email']);
                $message->subject(ucwords($data['name']).', let\'s Change Your Password. ');
            });

            return back()->with('success' , 'We have sent you password reset link. Please check your Mailbox');


        } else{
            return redirect('/password/reset');
        }


    }

}
