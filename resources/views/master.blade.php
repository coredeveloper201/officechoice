<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('page-title')</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('office/icons/themify-icons.css')}}">

	{{--<link rel="stylesheet" href="{{asset('office/css/default.css')}}">--}}
	<link rel="stylesheet" href="{{asset('office/js/bootstrap/css/bootstrap.css')}}">


	{{--<link rel="stylesheet" href="{{asset('office/css/style.css')}}">--}}
	<link rel="stylesheet" href="{{asset('office/css/custom.css')}}">
	<link rel="stylesheet" href="{{asset('office/js/scroll-slider/slider.css')}}">

	<script src="{{asset('office/js/jquery-1.11.1.min.js')}}"></script>
	<!--<script src="assets/bootstrap/js/bootstrap.min.js"></script>-->
	<script src="{{asset('office/js/jquery.backstretch.min.js')}}"></script>
	<script src="{{asset('office/js/custom.js')}}"></script>
	<script src="{{asset('office/js/imagezoom.js')}}"></script>
	<script src="{{asset('office/js/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('office/js/scroll-slider/slider.min.js')}}"></script>

	<script src="{{asset('office/js/jquery.backstretch.min.js')}}"></script>

	<style>
		.downloads a {
			color: #ffffff;
		}
		.has{display:none;
			#display: block;
			position: absolute;
			width: 30%;
			margin:0 auto;
			box-shadow: 25px 37px 98px 32px;
			left: 35%;
			top:10%;
			z-index:1000;
		}

		.panel{
			margin-bottom:0px!important;
		}

		.notification-popup-container-main {
			#position: absolute;
			top: 10%;

			margin:0 auto;
			width: 30%;
			height: auto;
			background: #ccc;
			#z-index: 100;
			box-shadow: 10px 10px 10px 10px #CCC;
			display:none;
			#transition: top 300ms cubic-bezier(0.17, 0.04, 0.03,
		}

		.notification-popup-container {
			background-color: #fff;
			border: 1px solid black;
			#-webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
			overflow: visible;
			width: 400px;
			z-index: 99;
			display: none;
		}

		.notification-popup-body {
			#padding: 10px 0px 0px 0px !important;
		}

		.notification-type {
			#background-color: red;
		}
		.panel{
			margin-bottom:0px!important;
		}

	</style>


</head>
<body id="searchp" class="active">

	@include('header')

	@yield('mainContent')

	@include('component.supl')


</body>
</html>