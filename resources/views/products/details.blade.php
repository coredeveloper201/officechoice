@extends('master')


@section('page-title')

    {{$Item->internetItemName}}
@endsection

@section('mainContent')
    <style>.searchblock{display:none}</style>

    @include('component.downloadpupup')

    <div class= "container pageblock">
        @include('component.details')
    </div>

    <div class="container searchblock">
        @include('component.searchBlock')
    </div>

    @include('component.print-div')

    @include('component.searchScript')


@endsection