@extends('master')


@section('page-title')

    Home
@endsection

@section('mainContent')


    <div class="container pageblock">
        <div class="breadcrumb">
            <a href="product.php">Home</a> > <a href="detail.php?code=506766"> STABILO 842 41 OHP PEN Permanent Fine
                Blue </a>
        </div>


        <div class="contant">
            <div class="row">
                <div class="product-details">
                    <!-- Left Page Column -->
                    <div class="product-image col-md-3 col-sm-12">

                        <!-- Images -->
                        <div class="thumbnil">

                            <!-- Main Image -->
                            <div class="image">
                                <!-- <span class="zoom-icon"> <img src="./icon/zoom-icon.png" alt="zoom image" title="zoom image"> Zoom</span> -->
                                <img class="zoom_image" id="img_01" src="{{asset('office/imageItemServer/3.jpg')}}"
                                     data-zoom-image="{{asset('office/imageItemServer/3.jpg')}}"/>
                            </div>
                            <!-- More Image Gallery -->
                            <script>
                                $("#img_01").elevateZoom({
                                    gallery: 'additional_image',
                                    cursor: 'pointer',
                                    galleryActiveClass: 'active',
                                    imageCrossfade: true,
                                    zoomType: 'window'
                                });

                                //pass the images to Fancybox
                                $("#img_01").bind("click", function (e) {
                                    var ez = $('#img_01').data('elevateZoom');
                                    $.fancybox(ez.getGalleryList());
                                    return false;
                                });

                                $(document).ready(function () {
                                    $('.additional_image').bxSlider({
                                        slideWidth: 80,
                                        minSlides: 2,
                                        maxSlides: 3,
                                        slideMargin: 10,
                                    });
                                });
                            </script>
                        </div>


                        <div class="bottom-link">
                            <table width="100%" border="0">
                                <thead>
                                <tr>
                                    <td>Video</td>
                                    <td>MSDS</td>
                                    <td>Link</td>
                                </tr>
                                </thead>
                                <tr>
                                    <td>
                                        <img class="iconsize" src="{{asset('office/image/video.jpg')}}"
                                             alt="No Content Available" title="No Content Available"/>
                                    </td>
                                    <td>
                                        <img class="iconsize" src="{{asset('office/image/msds.jpg')}}"
                                             alt="No Content Available" title="No Content Available"/>

                                    </td>
                                    <td>
                                        <img class="iconsize" src="{{asset('office/image/link.jpg')}}"
                                             alt="No Content Available" title="No Content Available"/>


                                    </td>
                                </tr>
                            </table>
                        </div>

                        <!-- Product Status Icons -->
                        <div class="bottom-status">
                            <table width="100%" border="0">
                                <thead>
                                <tr>
                                    <td>Managed</td>
                                    <td>OC Only</td>
                                    <td>Status</td>
                                    <td>Consumable</td>
                                    <td>Print</td>
                                </tr>
                                </thead>
                                <tr>
                                    <td>
                                        <img class="iconsize2" alt="Not Managed" title="Not Managed"
                                             src="{{asset('office/image/cross-sign.jpg')}}"/>
                                    </td>
                                    <td>
                                        <img class="iconsize2" alt="Not OC Only" title="Not OC Only"
                                             src="{{asset('office/image/cross-sign.jpg')}}"/>
                                    </td>
                                    <td>
                                        <img class="iconsize2" alt="Is active" title="Is active"
                                             src="{{asset('office/image/right-sign.jpg')}}"/>

                                    </td>
                                    <td>
                                        <img class="iconsize2" alt="Not Consumable" title="Not Consumable"
                                             src="{{asset('office/image/cross-sign.jpg')}}"/>
                                    </td>
                                    <td>
                                        <img style="padding-top:5px" class="iconsize2 print" alt="Print"
                                             onClick="printdiv('print-div');" title="Print"
                                             src="{{asset('office/image/icon-print.png')}}"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- Product Description -->
                    <div class="product-details col-md-9 col-sm-12">
                        <h2 class="product-name">
                            STABILO 842 41 OHP PEN Permanent Fine Blue </h2>

                        <div class="clear"></div>
                        <ul class="list-inline bottol-border">
                            <li><strong>OC Code :</strong> 506766</li>
                            <li><strong>Barcode :</strong> 4006381119061</li>
                            <li><strong>Manf :</strong> 0096420</li>
                            <li><strong>Old Code :</strong> ACO-0096420</li>
                        </ul>

                        <div class="clear"></div>
                        <div class="det-left col-md-7 col-sm-12">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab-basic" data-toggle="tab">Basic</a></li>
                                <li><a href="#tab-advanced" data-toggle="tab">Advanced</a></li>
                            </ul>

                            <!-- TABS -->
                            <div class="tab-content">

                                <!-- Basic Tab -->
                                <div class="tab-pane active" id="tab-basic">
                                    <div class="clear"></div>
                                    <table width="100%" border="0" class="skip-space">
                                        <tr>
                                            <td><strong>UOM :</strong></td>
                                            <td>BOX</td>
                                            <td><strong>Weight :</strong></td>
                                            <td></td>
                                            <td><strong>Dimenson :</strong></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><strong>FLC :</strong></td>
                                            <td>No</td>
                                            <td><strong>EDU :</strong></td>
                                            <td>no</td>
                                            <td><strong>
                                                    Active : </strong>
                                            </td>
                                            <td>27-Jan-16</td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" class="pt15"><strong>Internet Menu :</strong></td>
                                        </tr>
                                    </table>

                                    <div class="clear"></div>
                                    <p>
                                    <ul class="bulletdesc">
                                    </ul>
                                    <br>
                                </div>

                                <!-- Advanced Tab -->
                                <div class="tab-pane" id="tab-advanced">
                                    <div class="clear"></div>
                                    <table width="100%" border="0" class="cost-price">
                                        <thead>
                                        <tr>
                                            <td colspan="2"><strong>Meta Data</strong></td>
                                            <td colspan="2"><strong>Merchandise Hierarchy</strong></td>
                                        </tr>
                                        </thead>
                                        <tr>
                                            <td><strong>Product Type:</strong></td>
                                            <td></td>
                                            <td><strong>Department:</strong></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Product Cat:</strong></td>
                                            <td></td>
                                            <td><strong>Major Class:</strong></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Brand:</strong></td>
                                            <td></td>
                                            <td><strong>Minor Class:</strong></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Colour:</strong></td>
                                            <td></td>
                                            <td><strong>Sub Class:</strong></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Size</strong></td>
                                            <td></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Weight:</strong></td>
                                            <td></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Capacity:</strong></td>
                                            <td></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Dimensions:</strong></td>
                                            <td></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Certification:</strong></td>
                                            <td></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Pack Qty:</strong></td>
                                            <td></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                    </table>
                                </div>

                                <!-- Promotion Tab -->


                                <!-- Available Suppliers List -->
                                <div class="available-suppliers">
                                    <table width="100%" border="0">
                                        <thead>
                                        <tr>
                                            <td colspan="9" class="text-center"><strong>AVAILABLE SUPPLIERS</strong>
                                            </td>
                                        </tr>
                                        </thead>
                                        <thead>
                                        <tr>
                                            <td>Supplier</td>
                                            <td>Code</td>
                                            <td>Uom</td>
                                            <td>MOQ</td>
                                            <td>Div.</td>
                                            <td>Cost</td>
                                            <td>Unit</td>
                                            <td>Special</td>
                                            <td>From</td>
                                            <td>To</td>
                                        </tr>
                                        </thead>

                                        <tr>
                                            <td><a href="./supplier.php?suppliercode=ACO&supplieritem=0096420">Acco
                                                    Australia</a></td>
                                            <td>0096420</td>
                                            <td>BOX</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>$16.06</td>
                                            <td><strong>$16.06</strong></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- Pricing Section -->
                        <div class="det-rightx auto-width col-md-5 col-sm-12">
                            <h4 class="price">Pricing</h4>
                            <hr/>
                            <h4 class="price all-caps">Suggested Sell Pricing</h4>

                            <table width="100%" border="0" class="pricing-table">
                                <thead>
                                <tr>
                                    <td>Quantity Breaks</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tr>
                                    <td>Inc GST</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Ex GST</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>GP %</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Average GP %</td>
                                    <td colspan="4"></td>
                                </tr>
                            </table>
                            <br/>


                        </div>
                    </div>
                </div>
            </div>


            <div class="download4"></div>
        </div>
    </div>
    </div>
    </div>


@endsection
