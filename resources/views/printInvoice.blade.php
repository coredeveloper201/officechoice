
	<link rel="stylesheet" href="{{asset('office/css/style.css')}}">

<div id="print-div">
	<div style="width:300px; margin:0 auto;"><img src="{{asset('office/image/logo-s.png')}}" alt="Office Choice Logo"/></div>
<div class="product-details col-md-9 col-sm-12" style="padding-top:20px;padding-bottom: :20px;">

	<h2 style="color:#D0241A!important; text-align: center; "class="product-name">
		STABILO 842 41 OHP PEN Permanent Fine Blue 
	</h2>

	<div class="clear"> </div>
		<ul class="list-inline bottol-border"  style="padding-left: 0; margin-left: -5px; list-style: none; text-align: center; ">
			<li style="display: inline-block; padding-right: 5px; padding-left: 5px;"> <strong>OC Code :</strong> 506766</li>
			<li style="display: inline-block; padding-right: 5px; padding-left: 5px;"> <strong>Barcode :</strong> 4006381119061</li>
			<li style="display: inline-block; padding-right: 5px; padding-left: 5px;"> <strong>Manf :</strong> 0096420</li>
			<li style="display: inline-block; padding-right: 5px; padding-left: 5px;"> <strong>Old Code :</strong> ACO-0096420</li>
		</ul>	
</div>
<div style="width: 21cm;padding-top:20px; text-align: center; ">
	



<div style="width:104%;float: left;padding-left: 108px;margin-left:  551px;">
	<div style="border:1px solid #555555;padding:10px;">
		<table width="100%" border="0" class="skip-space">
	  <tr>
	    <td> <strong>UOM :</strong> </td>
	    <td>BOX</td>
	    <td> <strong>Weight :</strong> </td>
	     <td>  </td>
	      <td> <strong>Dimensions :</strong></td>
	    <td></td>
	     
	  
	  </tr>
	  <tr>
	   
	    <td><strong>FLC :</strong></td>
	      <td></td>
	       <td><strong>EDU :</strong></td>
	    <td>no</td>
	    <td><strong>Active On :</strong></td>
	      <td>27-Jan-2016</td>  
	  </tr>
	  <tr>
	    <td colspan="6" class="pt15"><strong>Internet Menu :</strong> </td>
	  </tr>
	</table>
	<div class="clear"> </div>

	<ul class="bulletdesc">
	           </ul>
	<br>
	</div>


<h1 style="color:#D0241A!important;"class="price all-caps">Suggested Sell Pricing</h1>
	<table width="100%" border="0" class="pricing-table">
	  <thead>
	  <tr style="background-color: gray!important;padding:20px">
	    <td >Quantity Breaks</td>
	    <td></td>
	    <td></td>
	    <td></td>
	    <td></td>
	  </tr>
	  </thead>
	  <tr>
	    <td>Inc GST</td>
	    <td></td>
	    <td></td>
	    <td></td>
	    <td></td>
	  </tr>
	  <tr>
	    <td >Ex GST</td>
	    <td></td>
	    <td></td>
	    <td></td>
	    <td></td>
	  </tr>
	  <tr>
	    <td >GP %</td>
	    <td></td>
	    <td></td>
	    <td></td>
	    <td></td>
	  </tr>
	  <tr>
	  <td >RRP Inc GST</td>
	    <td>
			</td>
		
	    <td ></td>
		    <td>Avg GP%</td>
		
	    <td></td>
	  </tr>
	</table>
<br />


<h1 style="color:#D0241A!important;" class="price all-caps">Education Pricing</h1>

		<table width="100%" border="0" class="pricing-table">
		  <thead>
		  <tr style="background-color: gray!important;padding:20px">
		    <td>Levels</td>
		    <td>1</td>
		    <td>2</td>
		    <td>3</td>
		    <td>4</td>
		  </tr>
		  </thead>
		  <tr>
		    <td>Inc GST</td>
		    <td>
				
			</td>
		    <td>
				
			</td>
		    <td>
				
			</td>
		    <td>
				</td>
		  </tr>
		  <tr>

		    <td>Ex GST</td>
		    <td>
				
			</td>
		    <td>
				
			</td>
		    <td>
				
			</td>
		    <td>
				
			</td>
		  </tr>
		  <tr>
		    <td>GP %</td>

		    	<td></td>
		    <td></td>
		    <td></td>
		    <td></td>
		  </tr>
		  <tr>
		  <td>Average GP %</td>
			    <td colspan="4"></td>
		    </tr>
		</table>


				</div>			
			</div>
		</div>


</div>


