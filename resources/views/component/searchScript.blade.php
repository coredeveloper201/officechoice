<script>

    $('.allsearch').click(function(){
        var search= $('#searchval').val();
        if(!$.trim(search).length) {
            $("#table").html('<div class="row"><div class="col-md-12"><p class="bold-style">Type in any part of a product description to begin your search.</p></div></div>');
            return true;
        }

        $(".pageblock").empty();
        $(".searchblock").show();

        $("#table").html('<div class="row"><div class="col-md-12"><p class="bold-style"><img class="loader"  src="{{ asset('image/searching.gif')  }}" /></p></div></div>');

        var pages = '<option selected="selected" value="1">1</option>';

        var supplier= $('#supplier').val();

        var limit= $('.limit').val();
        var sorting= $('#sorting option:selected').val();
        var page= $('.paging').val();


        var allsupplier=  $("input[name='allsupplier']:checked").val();
        var oconly=  $("input[name='oconly']:checked").val();
        var discontinued=  $("input[name='discontinued']:checked").val();
        var currentcatalog=  $("input[name='currentcatalog']:checked").val();
        var allwords=  $("input[name='allwords']:checked").val();


        $(".productcount").empty("0");
        $(".paging").html(pages);

        $(".totpag").empty("1");
        $(".arnextl").empty();
        $(".arprevl").empty();

        var url = "<?= route('filter') ?>";

        jQuery.ajax({
            url: url,
            dataType: 'json',
            data: {
                "supplier": supplier,
                "search": search,
                "limit": limit,
                "allsupplier": allsupplier,
                "oconly": oconly,
                "discontinued": discontinued,
                "currentcatalog": currentcatalog,
                "sorting": sorting,
                "allwords": allwords,
                "page": page,
            },
            success: function(data){


                $(".productcount").html(data.total);
                $(".paging").html(data.pages);
                $("#table").html(data.items);
                $(".totpag").html(data.totpag);
                $(".arprevl").html(data.arprev);
                $(".arnextl").html(data.arnext);



            }
        });





    });



    $('#searchp').keypress(function (e) {
        if (e.which == 13) {
            $('.allsearch').click();
            return false;
        }
    });

    $(document).on("change",".paging",function(){

        $('.allsearch').click();

    });
    $(document).on("change","#supplier",function(){

        $('.allsearch').click();

    });

    $(document).on("change",".limit",function(){

        $('.allsearch').click();

    });
    $(document).on("change","#sorting",function(){
        $('.allsearch').click();
    });

    $(document).on("change","input[name='allsupplier']",function(){

        $('.allsearch').click();

    });
    $(document).on("change","input[name='oconly']",function(){

        $('.allsearch').click();

    });

    $(document).on("change","input[name='discontinued']",function(){

        $('.allsearch').click();

    });

    $(document).on("change","input[name='currentcatalog']",function(){

        $('.allsearch').click();

    });

    $(document).on("change","input[name='allwords']",function(){

        $('.allsearch').click();

    });

    $('.arprevl').click(function(){
        var num = +$(".paging").val() - 1;
        $(".paging").val(num);
        $('.allsearch').click();

    });

    $('.arnextl').click(function(){

        var num = +$(".paging").val() + 1;
        $(".paging").val(num);
        $('.allsearch').click();

    });







</script>
