<style>
    #print-div {
        width: 21cm;
        min-height: 29.7cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        display: none;
    }

</style>
<div id="print-div">
    <div style="width:300px; margin:0 auto;"><img src="{{ asset('image/logo-s.png')  }}" alt="Office Choice Logo"/></div>
    <div class="product-details col-md-9 col-sm-12" style="padding-top:20px;padding-bottom: :20px;">
        <h2 style="color:#D0241A!important;"class="product-name">
            <?php echo $Item['internetItemName']; ?>

        </h2>
        <div class="clear"> </div>
        <ul class="list-inline bottol-border">
            <li> <strong>OC Code :</strong> <?php echo $Item['objectId']; ?></li>
            <li> <strong>Barcode :</strong> <?php echo $Item['barcodeSellUOM']; ?></li>
            <li> <strong>Manf :</strong> <?php echo $Item['manufacturersItemCode']; ?></li>
            <li> <strong>Old Code :</strong> <?php echo $Item['itemCode']; ?></li>
        </ul>
    </div>
    <div style="width: 21cm;padding-top:20px;">
        <div style="width:20%;float:left">

            @php
                $itemCode = $Item->itemCode;
                    @endphp

            <?php $image =  asset("image/imageItemServer/".$itemCode."/".$itemCode.".jpg");
            echo '<img class="img-responsive" src="'.$image.'"/>';?>
        </div>
        <div style="width:70%;float:left;padding-left:20px;">
            <div style="border:1px solid #555555;padding:10px;">
                <table width="100%" border="0" class="skip-space">
                    <tr>
                        <td> <strong>UOM :</strong> </td>
                        <td><?php echo $Item['pricingSellUOM']; ?></td>
                        <td> <strong>Weight :</strong> </td>
                        <td> <?php if ($Item['productWeight']) { echo $Item['productWeight']  . "gm"; } ?> </td>
                        <td> <strong>Dimensions :</strong></td>
                        <td><?php if ($Item['productLength']) { echo $Item['productLength'] . "x". $Item['productWidth']. "x". $Item['productHeight'] . "cm"; } ?></td>


                    </tr>
                    <tr>

                        <td><strong>FLC :</strong></td>
                        <td></td>
                        <td><strong>EDU :</strong></td>
                        <td><?php echo $Item['EDUFlag']; ?></td>
                        <td><strong>Active On :</strong></td>
                        <td><?php echo date("d-M-Y", intval($Item['dateItemActive'])); ?></td>
                    </tr>
                    <tr>
                        <td colspan="6" class="pt15"><strong>Internet Menu :</strong> <?php echo $Item['internetMenu']; ?></td>
                    </tr>
                </table>
                <div class="clear"> </div>

                <ul class="bulletdesc">
                    <?php if ($Item['bulletPointOne']) { echo "<li>".$Item['bulletPointOne'].'</li>'; } ?>
                    <?php if ($Item['bulletPointTwo']) { echo "<li>".$Item['bulletPointTwo']. '</li>'; } ?>
                    <?php if ($Item['bulletPointThree']) { echo "<li>".$Item['bulletPointThree']. '</li>'; } ?>
                    <?php if ($Item['bulletPointFour']) { echo "<li>".$Item['bulletPointFour']. '</li>'; } ?>
                    <?php if ($Item['bulletPointFive']) { echo "<li>".$Item['bulletPointFive']. '</li>'; } ?>
                    <?php if ($Item['bulletPointSix']) { echo "<li>".$Item['bulletPointSix']. '</li>'; } ?>
                    <?php if ($Item['bulletPointSeven']) { echo "<li>".$Item['bulletPointSeven']. '</li>'; } ?>
                    <?php if ($Item['bulletPointEight']) { echo "<li>".$Item['bulletPointEight']. '</li>'; } ?>
                    <?php if ($Item['bulletPointNine']) { echo "<li>".$Item['bulletPointNine']. '</li>'; } ?>
                    <?php if ($Item['bulletPointTen']) { echo "<li>".$Item['bulletPointTen']. '</li>'; } ?>
                </ul>
                <br>
            </div>


            <h1 style="color:#D0241A!important;"class="price all-caps">Suggested Sell Pricing</h1>
            <table width="100%" border="0" class="pricing-table">

                @php
                    $divideBY = isset($sprefercost['itemCost1']) ? $sprefercost['itemCost1']/$sprefercost['divisor'] :  0;
                $priceBreakOne = $Item['priceBreakOne'] ? $Item['priceBreakOne'] : .11111;
                $priceBreakTwo = $Item['priceBreakTwo'] ? $Item['priceBreakTwo'] : .11111;
                $priceBreakThree = $Item['priceBreakThree'] ? $Item['priceBreakThree'] : .11111;
                $priceBreakFour = $Item['priceBreakFour'] ? $Item['priceBreakFour'] : .11111;
                @endphp

                <?php 	if ($Item['GSTTaxableFlag']=='yes') {
                    $gst=1.1;
                } else {
                    $gst=1;
                }
                ?>
                <thead>
                <tr style="background-color: gray!important;padding:20px">
                    <td >Quantity Breaks</td>
                    <td><?php $num = $Item['quantityBreakOne']; if ($num>0) { echo $num; } ?></td>
                    <td><?php $num = $Item['quantityBreakTwo']; if ($num>0) { echo $num; } ?></td>
                    <td><?php $num = $Item['quantityBreakThree']; if ($num>0) { echo $num; } ?></td>
                    <td><?php $num = $Item['quantityBreakFour'];  if ($num>0) { echo $num; } ?></td>
                </tr>
                </thead>
                <tr>
                    <td>Inc GST</td>
                    <td><?php $num = ($priceBreakOne*$gst); if ($num>0 && $priceBreakOne != .11111) { echo "$"; echo sprintf("%.2f",$num); } ?></td>
                    <td><?php $num = ($priceBreakTwo*$gst); if ($num>0 && $priceBreakOne != .11111) { echo "$"; echo sprintf("%.2f",$num); } ?></td>
                    <td><?php $num = ($priceBreakThree*$gst); if ($num>0 && $priceBreakOne != .11111) { echo "$"; echo sprintf("%.2f",$num); } ?></td>
                    <td><?php $num = ($priceBreakFour*$gst); if ($num>0 && $priceBreakOne != .11111) { echo "$"; echo sprintf("%.2f",$num); } ?></td>
                </tr>
                <tr>
                    <td >Ex GST</td>
                    <td><?php $num = $priceBreakOne; if ($num>0 && $priceBreakOne != .11111) { echo "$"; echo sprintf("%.2f",$num); } ?></td>
                    <td><?php $num = $priceBreakTwo; if ($num>0 && $priceBreakOne != .11111) { echo "$"; echo sprintf("%.2f",$num); } ?></td>
                    <td><?php $num = $priceBreakThree; if ($num>0 && $priceBreakOne != .11111) { echo "$"; echo sprintf("%.2f",$num); } ?></td>
                    <td><?php $num = $priceBreakFour; if ($num>0 && $priceBreakOne != .11111) { echo "$"; echo sprintf("%.2f",$num); } ?></td>
                </tr>
                <tr>
                    <td >GP %</td>
                    <td><?php $num = ($gp1= ($priceBreakOne- ($divideBY))/$priceBreakOne)*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; } ?></td>
                    <td><?php $num = ($gp2=($priceBreakTwo-  ($divideBY))/$priceBreakTwo)*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; } ?></td>
                    <td><?php $num = ($gp3=($priceBreakThree-($divideBY))/$priceBreakThree)*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; } ?></td>
                    <td><?php $num =  ($gp4=($priceBreakFour-($divideBY))/$priceBreakFour)*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; } ?></td>
                </tr>
                <tr>
                    <td >RRP Inc GST</td>
                    <td>
                        <?php if ($Item['RRP']>0) { ?>
	<?php $num = ($Item['RRP']*$gst); echo "$"; echo number_format((float)$num, 2, '.', ''); ?>
	<?php } ?>
                    </td>

                    <td ></td>
                    <?php $gpav =  (($gp1 + $gp2 + $gp3 + $gp4) / 4)*100;  ?>
                    <td>Avg GP% </td>

                    <td><?php if ($gpav>0) { echo number_format((float)$gpav, 2, '.', ''); echo "%"; } ?></td>
                </tr>
            </table>
            <br />

            <!-- End Education -->
            <?php if ($Item['educationPrice1']>0) { ?>
            <h1 style="color:#D0241A!important;" class="price all-caps">Education Pricing</h1>

            <table width="100%" border="0" class="pricing-table">
                <thead>
                <tr style="background-color: gray!important;padding:20px">
                    <td>Levels</td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                </tr>
                </thead>
                <tr>
                    <td>Inc GST</td>
                    <td>
                        <?php $num = $Item['educationPrice1']*$gst; if ($num>0) { echo "$"; echo sprintf("%.2f",$num); } ?>

                    </td>
                    <td>
                        <?php $num = $Item['educationPrice2']*$gst; if ($num>0) { echo "$"; echo sprintf("%.2f",$num); } ?>

                    </td>
                    <td>
                        <?php $num = $Item['educationPrice3']*$gst; if ($num>0) { echo "$"; echo sprintf("%.2f",$num); } ?>

                    </td>
                    <td>
                        <?php $num = $Item['educationPrice4']*$gst; if ($num>0) { echo "$"; echo sprintf("%.2f",$num); } ?>
                    </td>
                </tr>
                <tr>

                    <td>Ex GST</td>
                    <td>
                        <?php $num = $Item['educationPrice1']; if ($num>0) { echo "$"; echo sprintf("%.2f",$num); } ?>

                    </td>
                    <td>
                        <?php $num = $Item['educationPrice2']; if ($num>0) { echo "$"; echo sprintf("%.2f",$num); } ?>

                    </td>
                    <td>
                        <?php $num = $Item['educationPrice3']; if ($num>0) { echo "$"; echo sprintf("%.2f",$num); } ?>

                    </td>
                    <td>
                        <?php $num = $Item['educationPrice4']; if ($num>0) { echo "$"; echo sprintf("%.2f",$num); } ?>

                    </td>
                </tr>
                <tr>
                    <td>GP %</td>

                    <?php $divi=0; ?>
                    <td><?php $num= ($gp1= ($Item['educationPrice1']-($divideBY))/$Item['educationPrice1'])*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; $divi++;} ?></td>
                    <td><?php $num= ($gp2= ($Item['educationPrice2']-($divideBY))/$Item['educationPrice2'])*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; $divi++;} ?></td>
                    <td><?php $num= ($gp3= ($Item['educationPrice3']-($divideBY))/$Item['educationPrice3'])*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; $divi++;} ?></td>
                    <td><?php $num= ($gp4= ($Item['educationPrice4']-($divideBY))/$Item['educationPrice4'])*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; $divi++;} ?></td>
                </tr>
                <tr>
                    <td>Average GP %</td>
                    <?php if($divi==0){$divi=1;} ?>
                    <td colspan="4"><?php $num = (($gp1 + $gp2 + $gp3 + $gp4) / $divi)*100; if ($num>0) { echo number_format((float)$num, 2, '.', ''); echo "%"; } ?></td>
                </tr>
            </table>

        <?php } ?>
        <!-- End Education Pricing -->
        </div>
    </div>
</div>
