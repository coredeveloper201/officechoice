<div class="breadcrumb">
    <a href="{{ route('products')  }}"><b>Home</b></a> >
    <a href="{{ route('product.details' , ['code' => $Item->objectId])  }}"><b> {{$Item->internetItemName}} </b></a>
    >
    <a href="{{route('supplier-item' , ['suppliercode' => $suppliercode , 'supplieritem' => $supplieritem ,'itemCode'=>$Item->objectId])}}"><b class="text-danger">{{$dbdata['supplierName']}}</b></a>
</div>
<div class="contant">
    <div class="row">
        <div class="product-details">
            <div class="product-image col-md-3 col-sm-12">

                <!-- Images -->
                <div class="thumbnil">

                    <!-- Main Image -->
                    <div class="image">
                        {{--                        <span class="zoom-icon"> <img src="./icon/zoom-icon.png" alt="zoom image" title="zoom image"> Zoom</span>--}}
                        @if(file_exists(public_path().'/image/imageItemServer/' . $Item->itemCode . '/' . $Item->itemCode . '.jpg'))
                            <img class="zoom_image" id="img_01"
                                 src="{{asset('image/imageItemServer/' . $Item->itemCode . '/' . $Item->itemCode . '.jpg')}}"
                                 data-zoom-image="{{asset('image/imageItemServer/' . $Item->itemCode . '/' . $Item->itemCode . '.jpg')}}"/>
                        @else
                            <img class="zoom_image" id="img_01"
                                 src="{{asset('image/imageItemServer/NoImage.jpg')}}"
                                 data-zoom-image="{{asset('image/imageItemServer/NoImage.jpg')}}"/>
                        @endif
                    </div>
                    <!-- More Image Gallery -->
                    <div class="additional_image" id="additional_image">
                        @foreach($imagefiles as $image)
                            <div>
                                <a href="#" data-image="{{$image['image']}}" data-zoom-image="{{$image['image']}}">
                                    <img id="img_01" src='{{$image['image']}}' data-zoom-image="{{$image['image']}}"/>
                                </a>

                            </div>
                        @endforeach
                    </div>

                    <script>
                        $("#img_01").elevateZoom({
                            gallery: 'additional_image',
                            cursor: 'pointer',
                            galleryActiveClass: 'active',
                            imageCrossfade: true,
                            zoomType: 'window'
                        });

                        //pass the images to Fancybox
                        $("#img_01").bind("click", function (e) {
                            var ez = $('#img_01').data('elevateZoom');
                            $.fancybox(ez.getGalleryList());
                            return false;
                        });

                        $(document).ready(function () {
                            $('.additional_image').bxSlider({
                                slideWidth: 80,
                                minSlides: 2,
                                maxSlides: 3,
                                slideMargin: 10,
                            });
                        });
                    </script>
                </div>


                <div class="bottom-link">
                    <table width="100%" border="0">
                        <thead>
                        <tr>
                            <td>Video</td>
                            <td>MSDS</td>
                            <td>Link</td>
                        </tr>
                        </thead>
                        <tr>

                            <td>
                                @if($Item->productVideoLink)
                                    <a href="{{$Item->productVideoLink}}">
                                        <img class="iconsize" src="{{asset('office/image/video.jpg')}}"
                                             alt="No Content Available" title="No Content Available"/>
                                    </a>
                                @else

                                    <img class="iconsize" src="{{asset('office/image/video.jpg')}}"
                                         alt="No Content Available" title="No Content Available"/>
                                @endif

                            </td>
                            <td>
                                @if($Item->PDFLink)
                                    <a href="{{$Item->PDFLink}}">
                                        <img class="iconsize" src="{{asset('office/image/msds.jpg')}}"
                                             alt="No Content Available" title="No Content Available"/>
                                    </a>
                                @else
                                    <img class="iconsize" src="{{asset('office/image/msds.jpg')}}"
                                         alt="No Content Available" title="No Content Available"/>
                                @endif


                            </td>
                            <td>
                                @if($Item->productWebLink)
                                    <a href="{{$Item->productWebLink}}">
                                        <img class="iconsize" src="{{asset('office/image/link.jpg')}}"
                                             alt="No Content Available" title="No Content Available"/>
                                    </a>
                                @else

                                    <img class="iconsize" src="{{asset('office/image/link.jpg')}}"
                                         alt="No Content Available" title="No Content Available"/>
                                @endif

                            </td>
                        </tr>
                    </table>
                </div>

                <!-- Product Status Icons -->
                <div class="bottom-status">
                    <table width="100%" border="0">
                        <thead>
                        <tr>
                            <td>Managed</td>
                            <td>OC Only</td>
                            <td>Status</td>
                            <td>Consumable</td>
                            <td>Print</td>
                        </tr>
                        </thead>
                        @if(isset($Item))
                            <tr>
                                <td>
                                    @if($Item->managedRangeFlag == 'yes')
                                        <img class="iconsize2" alt="Managed" title="Not Managed"
                                             src="{{asset('office/image/right-sign.jpg')}}"/>
                                    @else
                                        <img class="iconsize2" alt="Not Managed" title="Not Managed"
                                             src="{{asset('office/image/cross-sign.jpg')}}"/>
                                    @endif
                                </td>
                                <td>
                                    @if($Item->availability == 'oconly')
                                        <img class="iconsize2" alt="Managed" title="Not Managed"
                                             src="{{asset('office/image/right-sign.jpg')}}"/>
                                    @else
                                        <img class="iconsize2" alt="Not Managed" title="Not Managed"
                                             src="{{asset('office/image/cross-sign.jpg')}}"/>
                                    @endif
                                </td>
                                <td>
                                    @if($Item->productStatus == 'active')
                                        <img class="iconsize2" alt="Is active" title="Is active"
                                             src="{{asset('office/image/right-sign.jpg')}}"/>
                                    @else
                                        <img class="iconsize2" alt="Not active" title="Not active"
                                             src="{{asset('office/image/cross-sign.jpg')}}"/>
                                    @endif
                                </td>
                                <td>
                                    @if($Item->consumableFlag == 'yes')
                                        <img class="iconsize2" alt="Consumable" title="Consumable"
                                             src="{{asset('office/image/right-sign.jpg')}}"/>
                                    @else
                                        <img class="iconsize2" alt="Not Consumable" title="Not Consumable"
                                             src="{{asset('office/image/cross-sign.jpg')}}"/>
                                    @endif
                                </td>
                                <td>
                                    <img style="padding-top:5px" class="iconsize2 print" alt="Print"
                                         onClick="printdiv('print-div');" title="Print"
                                         src="{{asset('office/image/icon-print.png')}}"/>
                                </td>
                            </tr>
                        @else
                            <tr><td class="text-center" colspan="5">Data Not Found</td></tr>
                        @endif
                    </table>
                </div>
            </div>
            <div class="product-details col-md-9 col-sm-12">
                <h2 class="product-name">
                    <?php echo $dbdata['supplierName']; ?>
                </h2>
                <div class="clear"></div>
                <ul class="list-inline bottol-border">
                    <li><strong>OC Code :</strong> {{$Item->objectId}}</li>
                    <li><strong>Barcode :</strong> {{$Item->barcodeSellUOM}}</li>
                    <li><strong>Manf :</strong> {{$Item->manufacturersItemCode}}</li>
                    <li><strong>Old Code :</strong> {{$Item->itemCode}}</li>
                </ul>
                <div class="clear"></div>

                <div class="det-left col-md-12 col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-item" data-toggle="tab">Item</a></li>
                        <li><a href="#tab-supplier" data-toggle="tab">Supplier</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-item">
                            <div class="det-leftb">
                                <table width="100%" border="0" class="cost-price">
                                    <thead>
                                    <tr>
                                        <td colspan="2"><strong>Details</strong></td>
                                        <td colspan="2"><strong>Cost/Price</strong></td>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td><strong>Supplier Code:</strong></td>
                                        <td><?php echo $dbdata['itemSupplierCode']; ?></td>
                                        <td><strong>Cost1:</strong></td>
                                        <td><?php $num = $dbdata['itemCost1']; if ($num > 0) {
                                                echo "$";
                                                echo sprintf("%.2f", $num);
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Barcode:</strong></td>
                                        <td>{{$Item->barcodeSellUOM ? $Item->barcodeSellUOM : '0000'}}</td>
                                        <td><strong>Cost2:</strong></td>
                                        <td><?php $num = $Item->itemCost2;
                                            if ($num > 0) {
                                                echo "$";
                                                echo sprintf("%.2f", $num);
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Buy UOM:</strong></td>
                                        <td><?php echo $dbdata['buyUom']; ?></td>
                                        <td><strong>Cost3:</strong></td>
                                        <td><?php $num = $dbdata['itemCost3']; if ($num > 0) {
                                                echo "$";
                                                echo sprintf("%.2f", $num);
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Divisor:</strong></td>
                                        <td><?php echo $dbdata['divisor']; ?></td>
                                        <td><strong>Cost4:</strong></td>
                                        <td><?php $num = $dbdata['itemCost4']; if ($num > 0) {
                                                echo "$";
                                                echo sprintf("%.2f", $num);
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>MOQ</strong></td>
                                        <td><?php echo $dbdata['moq']; ?></td>
                                        <td><strong>Cost5:</strong></td>
                                        <td><?php $num = $dbdata['itemCost5']; if ($num > 0) {
                                                echo "$";
                                                echo sprintf("%.2f", $num);
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Weight:</strong></td>
                                        <td>{{$Item->productWeight ? $Item->productWeight : '0'}} gm</td>
                                        <td><strong>Promo Cost:</strong></td>
                                        <td><?php $num = $dbdata['itemCost6']; if ($num > 0) {
                                                echo "$";
                                                echo sprintf("%.2f", $num);
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Dimensions:</strong></td>
                                        <td>{{$Item->productLength ? $Item['productLength'] . "x". $Item['productWidth']. "x". $Item['productHeight'] . "cm" : ''}}</td>
                                        <td><strong>Future Cost:</strong></td>
                                        <td><?php $num = $dbdata['futureCost']; if ($num > 0) {
                                                echo "$";
                                                echo sprintf("%.2f", $num);
                                            } ?></td>
                                    </tr>



                                    <tr>
                                        <td><strong>Priority:</strong></td>
                                        <td><?php echo $dbdata['supplierItemPriority']; ?></td>
                                        <td><strong>FC Effective:</strong></td>
                                        <td><?php echo $fcDate; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Status:</strong></td>
                                        <td><?php echo $dbdata['productStatus']; ?></td>
                                        <td><strong>Special Cost:</strong></td>
                                        <td><?php $num = $dbdata['specialCost']; if ($num > 0) {
                                                echo "$";
                                                echo sprintf("%.2f", $num);
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td><strong>Sc Effective Form:</strong></td>
                                        <td><?php echo $scDateFrom; ?></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td><strong>Sc Effective To:</strong></td>
                                        <td><?php echo $scDateTo; ?></td>
                                    </tr>
                                </table>

                            </div>
                            <div class="det-right">
                                <h4 class="price all-caps">CHANGE HISTORY</h4>
                                <table width="100%" border="0" class="change-history all-caps">
                                    <thead>
                                    <tr>
                                        <td>Type</td>
                                        <td>Updated on</td>
                                        <td>Old</td>
                                        <td>New</td>
                                    </tr>
                                    </thead>
                                    <?php $i = 0; ?>
                                    <?php foreach ($histNew as $hist) { ?>

                                    <?php if ($i <= 4) { ?>
                                    <tr>
                                        <td><?php echo $histType[$i]; ?></td>
                                        <td><?php if (!empty($histDate[$i])) {echo date("d-M-Y", strtotime('+7 hour', $histDate[$i]));} ?></td>
                                        <td><?php echo $histOld[$i]; ?></td>
                                        <td><?php echo $histNew[$i]; ?></td>
                                    </tr>
                                    <?php $i = $i + 1; ?>
                                    <?php } ?>
                                    <?php } ?>
                                </table>

                            </div>
                        </div>
                        <div class="tab-pane specdiv" id="tab-supplier">

                            <div class="firstdiv">
                                <table class="full-cell title-heading" id="tablex" border="0" class="cost-price ">
                                    <tr><td  colspan="2"><strong>Address & Contact</strong></td></tr>
                                    <tr>
                                        <td><strong>Name:</strong></td>
                                        <td> <?php echo $dbdata['supplierName']; ?> </td>
                                    </tr>
                                    <tr>
                                        <td><strong>ABN:</strong></td>
                                        <td> <?php echo $dbdata['ABN']; ?> </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Address:</strong></td>
                                        <td> <?php echo $dbdata['address1']; ?></td>
                                    </tr>

                                    <?php if($dbdata['address2']){ ?>
                                    <tr>
                                        <td><strong>Address 2:</strong></td>
                                        <td> <?php echo $dbdata['address2']; ?> </td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td><strong>Suburb:</strong></td>
                                        <td>  <?php echo $dbdata['suburb']; ?> </td>
                                    </tr>

                                    <tr>
                                        <td><strong>State/PC:</strong></td>
                                        <td> <?php echo $dbdata['stateCode'] . " " . $dbdata['postCode']; ?> </td>
                                    </tr>

                                    <tr>
                                        <td><strong>Phone:</strong></td>
                                        <td> <?php echo $dbdata['phone']; ?> </td>
                                    </tr>

                                    <tr>
                                        <td><strong>Fax:</strong></td>
                                        <td>  <?php echo $dbdata['fax']; ?> </td>
                                    </tr>

                                    <tr>
                                        <td><strong>Web:</strong></td>
                                        <td>  <?php echo $dbdata['webAddress']; ?> </td>
                                    </tr>
                                </table>

                                <br><br>
                                <table id="tablez" border="0" class="cost-price">
                                    <tr>
                                        <td colspan="2" class="full-cell title-heading"><strong>Contact</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Contact Name:</strong></td>
                                        <td><?php echo $dbdata['contactName']; ?></td>
                                    </tr>

                                    <tr>
                                        <td><strong>Position:</strong></td>
                                        <td><?php echo $dbdata['contactPosition']; ?> </td>
                                    </tr>

                                    <tr>
                                        <td><strong>Mobile:</strong></td>
                                        <td><?php echo $dbdata['contactMobile']; ?> </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Email</strong></td>
                                        <td><?php echo $dbdata['contactEmail']; ?> </td>
                                    </tr>

                                </table>
                            </div>

                            <div class="seconddiv">
                                <table id="tabley" border="0" class="cost-price ">
                                    <thead>
                                    <tr>
                                        <td colspan="2"><strong>Trade & Terms</strong></td>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td><strong>Terms:</strong></td>
                                        <td><?php echo $dbdata['terms']; ?></td>
                                    </tr>

                                    <tr>
                                        <td><strong>Minimum Order:</strong></td>
                                        <td><?php echo $dbdata['minimum']; ?></td>
                                    </tr>

                                    <?php if($dbdata['eft']){ ?>
                                    <tr>
                                        <td><strong>EFT:</strong></td>
                                        <td><?php echo $dbdata['eft']; ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td><strong>Cards Accepted:</strong></td>
                                        <td><?php echo $dbdata['cards']; ?></td>
                                    </tr>

                                    <tr>
                                        <td><strong>Returns:</strong></td>
                                        <td><?php echo $dbdata['productReturns']; ?></td>
                                    </tr>

                                    <?php if($dbdata['dropShip']){ ?>
                                    <tr>
                                        <td><strong>DropShip:</strong></td>
                                        <td><?php echo $dbdata['dropShip']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="download4"></div>
</div>
