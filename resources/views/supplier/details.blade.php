@extends('master')


@section('page-title')

    Home
@endsection

@section('mainContent')
    <style>.searchblock{display:none}</style>

    @include('component.downloadpupup')

    <div class= "container pageblock">
        @include('component.supplier_details')
    </div>

    <div class="container searchblock">
        @include('component.searchBlock')
    </div>


    @include('component.searchScript')



@endsection